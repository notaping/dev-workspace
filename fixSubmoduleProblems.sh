#! /bin/bash
# This will set the branch of the submodules to the branch this workspaces has and update them accordingly (To the newest commits in the branch).
# This should *normally* not overwrite any changes you made.
# If you only want to match to branch name for a specific submodules, please do it manually by copy-pasting.

branchName=$(git symbolic-ref --short -q HEAD)
fixCommand="git pull --set-upstream origin $branchName; git checkout $branchName; git checkout head; git switch -"
# Do not add --recusrsive, as only level 1 submodules should be notaping modules (Except if i change this in the future)
git submodule foreach "$fixCommand"
This is a utility repo for development. When checked out via ``git clone --recurse-submodules git@gitlab.com:notaping/dev-workspace.git`` it will
contain a parent pom with all projects added as child projects. This makes development easier, as dependencies don't
need to be build separately to be accessed. Use ``git submodule update --remote`` to fetch changes to the submodules.  
If you experience issues with submodules, run fixSubmoduleProblems.sh.